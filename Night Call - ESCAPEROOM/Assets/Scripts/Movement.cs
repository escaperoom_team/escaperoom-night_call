﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    public float speed = 3.0f; //szybkość poruszania się gracza
    private CharacterController characterController; //referencja do komponentu postaci sterowalnej
    public float gravity = -9.8f; //uwzględnienie grawitacji (ziemska)
    //ROZPOCZĘCIE GRY
	void Start ()
    {
        characterController = GetComponent<CharacterController>(); //pobranie danych komponentu
	}
	//AKTUALIZACJA STANU CO KAŻDĄ KLATKĘ
	void Update ()
    {
        float deltaX = Input.GetAxis("Horizontal") * speed; //czeka i pobiera klawisze A,D oraz strzałek lewo,prawo
        float deltaZ = Input.GetAxis("Vertical") * speed; //czeka i pobiera klawisze W,S i strzałki góra, dół
        Vector3 movement = new Vector3(deltaX, 0, deltaZ); //inicjalizacja wektora odpowiadającego za przesuwanie gracza
        movement = Vector3.ClampMagnitude(movement, speed); //wyznaczenie granicy szybkości (nie bedziesz się poruszał szybciej niż 'speed')
        movement.y = gravity; //imitacja grawitacji
        movement = movement * Time.deltaTime; //uwzględnienie czasu
        movement = transform.TransformDirection(movement); //przesuwanie się w kierunku odbieranym przez input
        characterController.Move(movement); //przesuwanie się uwzględniające atrybuty komponentu char.contr
	}
}
